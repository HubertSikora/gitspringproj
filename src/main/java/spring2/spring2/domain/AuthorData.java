package spring2.spring2.domain;

import java.time.LocalDateTime;

public class AuthorData {

    private String name;
    private String email;
    private LocalDateTime date;
    private boolean id;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public LocalDateTime getDate() {
        return date;
    }

    public void setDate(LocalDateTime date) {
        this.date = date;
    }

    public boolean getId() {
        return id;
    }
}
