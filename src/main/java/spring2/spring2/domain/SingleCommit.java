package spring2.spring2.domain;

import org.springframework.stereotype.Component;

@Component //musimy oznaczyc jako komponent aby Spring wiedzial ze te klasy istnieja i moze do nich mapować.
public class SingleCommit {
    private AuthorData authorData;
    private AuthorData commiterData;
    private String message;

    public AuthorData getAuthorData() {
        return authorData;
    }

    public void setAuthorData(AuthorData authorData) {
        this.authorData = authorData;
    }

    public AuthorData getCommiterData() {
        return commiterData;
    }

    public void setCommiterData(AuthorData commiterData) {
        this.commiterData = commiterData;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
