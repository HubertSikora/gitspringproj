package spring2.spring2.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;
import spring2.spring2.domain.CommitData;
import spring2.spring2.domain.GithubData;
import spring2.spring2.errorHandling.MyException;


import java.util.Arrays;
import java.util.List;

@Service
public class GithubService {
    private RestTemplate restTemplate;
   final private String URL = "https://api.github.com/repos/{owner}/{repo}";

    @Autowired
    public GithubService(RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
    }

    public GithubData getRepoByUserAndRepoName(String username, String repositoryName) {
        try {
            GithubData response = restTemplate.getForObject(URL,
                    GithubData.class, username, repositoryName);
            return response;
        } catch (HttpClientErrorException ex) {
            GithubData errorResponse = new GithubData();
            errorResponse.setError(ex.getMessage());
            return errorResponse;
        }
    }

    public List<CommitData> getCommitsByUserAndRepoName(String username, String repositoryName) {
        try {
            CommitData[] response = restTemplate.getForObject(URL + "/commits",
                    CommitData[].class, username, repositoryName);
            List<CommitData> commitDataList = Arrays.asList(response);
            return commitDataList.subList(0, 3);
        } catch (HttpClientErrorException ex) {
            throw new MyException(ex.getMessage());
        }
    }
}