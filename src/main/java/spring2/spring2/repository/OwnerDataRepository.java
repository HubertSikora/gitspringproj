package spring2.spring2.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import spring2.spring2.domain.OwnerData;

@Repository
public interface OwnerDataRepository extends JpaRepository<OwnerData, Long> {
}
