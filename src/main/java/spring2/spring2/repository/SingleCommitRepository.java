package spring2.spring2.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import spring2.spring2.domain.SingleCommit;

@Repository
public interface SingleCommitRepository extends JpaRepository<SingleCommit, Long> {
}